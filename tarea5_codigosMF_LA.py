#ejercicio 1
print("Ejercicio 1: Superficie de un cono")
r=input("Ingrese radio en cm: ")
h=input("Ingrese altura en cm: ")
a=3.14*(int(r)*int(h))
print("El valor de la superficie lateral del cono es: "+str(a)+ " cms.")

#ejercicio 2
print("Ejercicio 2: evaluar en orden")
a = 'b'
print(a)
print(a + 'b')
print(a + 'a')
print(a * 2 + 'b' * 3)
print(2 * (a + 'b'))

#ejercicio 3
print("Ejercicio 3: pedir nombre de persona y repetir 1000 veces")
nombre=input("Ingrese nombre: ")
final=nombre
i=0
while i < 999:
    final+=" "+nombre
    i+=1
print(final)

#ejercicio 4
print("Ejercicio 4: comparar edades")
p1=input("Ingrese la edad de persona 1: ")
p2=input("Ingrese la edad de persona 2: ")
if int(p1)>int(p2):
    print("Persona 1 es mayor")
if int(p2)>int(p1):
    print("Persona 2 es mayor")
if int(p1)==int(p2):
    print("Ambas personas tienen la misma edad")

#ejercicio 5
print("Ejercicio 5: verificación de condición")
n1=input("Ingrese número 1: ")
n2=input("Ingrese número 2: ")
if int(n2)==int(n1)**2:
    print("El segundo es el cuadrado exacto del primero")
if int(n2)<int(n1)**2:
    print("El segundo es menor al cuadrado del primero")
if int(n2)>int(n1)**2:
    print("El segundo es mayor al cuadrado del primero")

#ejercicio 6
print("Ejercicio 6: pi")
#algoritmo de Chudnovsky

import decimal

n = int(input("Ingrese numero de decimales de Pi deseados: "))
def decimales_pi(n):
    decimal.getcontext().prec = n + 1
    a1 = decimal.Decimal(10005).sqrt()*426880
    cubo = 1
    ini = 13591409
    div = ini
    for i in range(1, n):
        mult = 1 * (6 ** 3 - 16 * 6) / ((i + 1) ** 3)
        ini += 545140134
        cubo *= -262537412640768000
        div += decimal.Decimal(mult * ini) / cubo
    pi = a1 / div
    return pi

print(decimales_pi(n))
